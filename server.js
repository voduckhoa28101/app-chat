const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);
const activeUsers = new Set();


app.get('/', (req, res) => {
  res.sendFile(__dirname + '/login.html');
});

app.get('/chat', (req, res) => {
  res.sendFile(__dirname + '/app.html');
});

io.on('connection', (client) => {
  // socket.on("new user", function (data) {
  //   socket.key = data
  //   activeUsers.add(data);
  //   io.emit("new user", [...activeUsers]);
  // });

  // socket.on("disconnect", () => {
  //   activeUsers.delete(socket.key);
  //   io.emit("user disconnected", socket.key);
  // })
  const username = client.handshake.auth.username;
  client.on("chat message", (msg) => {
    io.emit('chat message', {
      username: username,
      msg: msg
    });
  })
  activeUsers.add(username)
  io.emit("new connected", [...activeUsers]);
});

server.listen(4000, () => {
  console.log('listening on *:4000');
});